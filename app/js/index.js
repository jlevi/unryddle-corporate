(function($){
    'use strict';

    // define survey endpoint where results will be sent
    var surveyCorporateRef = firebase.database().ref('corporate_survey');
    var careerClusterRef = firebase.database().ref('career_clusters');
    function getCareerClusters(){
        var xhr = careerClusterRef.orderByChild('name').once('value');
        return xhr;
    }

    function getClusterSet(){
        var clusterSet = getCareerClusters();
        clusterSet.then(function(snapshot){
            var results = snapshot.val();
            populateSelectBox(results);
        }).catch(function(err){
            console.log(err.message);
        })
    }
    function populateSelectBox(obj){
        var options = '';
        const has = Object.prototype.hasOwnProperty;
        options = options.concat("<option value=''>Select Your Industry</option>");
        for(var item in obj){
            if(has.call(obj, item)){
                var clusterItem = obj[item];
                // var clusterID = item;
                options = options.concat('<option value="', clusterItem.name, '">', clusterItem.name, '</option>');
            }
        };
        // console.log(options);
        $('#select-industry').append(options);
    }



    // define options for success, error modals
    var modalOptions = {
        backdrop: true,
        keyboard: false,
        focus: true,
        show: true
    };

    
    // define validation rules for survey form
    $('#survey-form').validate({
        rules:{
            "talents_ranking": {
                required: true
            },
            "why_talent_ranking": {
                required: true,
                maxlength: 400
            },
            "key_talent_skills": {
                required: true
            },
            "most_important_skills": {
                required: true
            },
            "graduates_prepared": {
                required: true
            },
            "why_graduates_prepared":{
                required: true,
                maxlength: 400
            },
            "select-industry":{
                required: true
            }
        }
    });


    
    
    // form submission event handler
    $('#complete-survey').off('click').on('click', function(e) {
        e.preventDefault();
        
        // check if form is valid
        if($('#survey-form').valid()){
            
            // set object payload for survey endpoint store
            var payload = {
                areGraduatesPrepared: $('#graduates_prepared option:selected').val().trim(),
                currentTalentRankings: $('#talents_ranking option:selected').val().trim(),
                whyGraduatesPrepared: $('#why_graduates_prepared').val().trim(),
                mostImportantSkills: $('#most_important_skills').val().trim(),
                keyTalentSkills: $('#key_talent_skills').val().trim(),
                whyTalentRanking: $('#why_talent_ranking').val().trim(),
                targetIndustry: $('#select-industry option:selected').val()
            }
            // set unique key for survey endpoint
            var surveyCorporateKey = surveyCorporateRef.push().key;
            
            // write payload to survey endpoint with unique key set above
            firebase.database().ref("corporate_survey/" + surveyCorporateKey).set(payload, function(error){
                if(error) {
                    console.log(error);
                    $('#errorModal').modal(modalOptions);
                } else {
                    $('#successModal').modal(modalOptions);
                }
            });
        }
    });
    
    // clear input fields when modals are hidden
    $('#successModal').on('hidden.bs.modal', function(e) {
        $('#talents_ranking').val('');
        $('#graduates_prepared').val('');
        $('input, textarea').val('');
    });
    $('#errorModal').on('hidden.bs.modal', function(e) {
        $('#talents_ranking').val('');
        $('#graduates_prepared').val('');
        $('input, textarea').val('');
    });
    
    // function to get selected radio value from radio input object
    function getRadioVals(obj){
        var radioVal = null;
        obj.each(function() {
            var $this = $(this);
            if($this.prop('checked')) {
                radioVal = $this.val();
            }
        });
        
        return radioVal;
    }
    
    // else {
        // $('#invalid-form').removeClass('hide');
        // }
        
    // populate select box
    getClusterSet();
    }(jQuery));